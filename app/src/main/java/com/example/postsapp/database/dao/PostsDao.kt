package com.example.postsapp.database.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.example.postsapp.database.models.PostsTable

@Dao
interface PostsDao {
    @Query("SELECT * FROM posts_table")
    fun getAll(): MutableList<PostsTable>

    @Query("SELECT * FROM posts_table WHERE id=:id")
    fun getById(id: Long): PostsTable

    @Query("SELECT * FROM posts_table WHERE id IN (:userIds)")
    fun loadAllByIds(userIds: IntArray): List<PostsTable>

    //@Query("SELECT * FROM geos_table WHERE name LIKE :name AND " +
    //        "username LIKE :userName LIMIT 1")
    //fun findByName(name: String, userName: String): GeosTable

    @Insert
    fun insert(user: PostsTable): Long

    @Insert
    fun insertAll(vararg users: PostsTable)

    @Delete
    fun delete(user: PostsTable)
}