package com.example.postsapp.database.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "posts_table")
data class PostsTable(

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id") val id: Long = 0,
    @ColumnInfo(name = "id_user") var id_user: Long? = 0,
    @ColumnInfo(name = "id_server") var id_server: Long? = 0,
    @ColumnInfo(name = "tittle") var tittle: String? = "",
    @ColumnInfo(name = "body") var body: String? = ""

)