package com.example.postsapp.database.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "address_table")
data class AddressTable (

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id") val id: Long = 0,
    @ColumnInfo(name = "id_remote") var id_server: String? = "",
    @ColumnInfo(name = "id_geo") var id_geo: Long? = 0,
    @ColumnInfo(name = "city") var city: String? = "",
    @ColumnInfo(name = "street") var street: String? = "",
    @ColumnInfo(name = "suite") var suite: String? = "",
    @ColumnInfo(name = "zipcode") var zipcode: String? = ""

)