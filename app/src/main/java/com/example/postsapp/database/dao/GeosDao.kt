package com.example.postsapp.database.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.example.postsapp.database.models.GeosTable

@Dao
interface GeosDao {

    @Query("SELECT * FROM geos_table")
    fun getAll(): MutableList<GeosTable>

    @Query("SELECT * FROM geos_table WHERE id=:id")
    fun getById(id: Long): GeosTable

    @Query("SELECT * FROM geos_table WHERE id IN (:userIds)")
    fun loadAllByIds(userIds: IntArray): List<GeosTable>

    //@Query("SELECT * FROM geos_table WHERE name LIKE :name AND " +
    //        "username LIKE :userName LIMIT 1")
    //fun findByName(name: String, userName: String): GeosTable

    @Insert
    fun insert(user: GeosTable): Long

    //@Insert
    //fun insertAll(vararg users: UsersTable)

    @Delete
    fun delete(user: GeosTable)
}