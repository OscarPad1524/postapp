package com.example.postsapp.database.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.example.postsapp.database.models.CompanysTable

@Dao
interface CompanysDao {

    @Query("SELECT * FROM companys_table")
    fun getAll(): MutableList<CompanysTable>

    @Query("SELECT * FROM companys_table WHERE id=:id")
    fun getById(id: Long): CompanysTable

    @Query("SELECT * FROM companys_table WHERE id IN (:userIds)")
    fun loadAllByIds(userIds: IntArray): List<CompanysTable>

    //@Query("SELECT * FROM companys_table WHERE name LIKE :name AND " +
    //        "username LIKE :userName LIMIT 1")
    //fun findByName(name: String, userName: String): CompanysDao

    @Insert
    fun insert(user: CompanysTable): Long

    //@Insert
    //fun insertAll(vararg users: UsersTable)

    @Delete
    fun delete(user: CompanysTable)

}