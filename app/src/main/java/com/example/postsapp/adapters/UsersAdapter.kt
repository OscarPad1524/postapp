package com.example.postsapp.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.postsapp.R
import com.example.postsapp.database.models.UsersTable

class UsersAdapter(
    val lista: List<UsersTable>,
    val context: Context,
    var onClic: (UsersTable, Int) -> Unit
): RecyclerView.Adapter<UsersAdapter.UsersHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UsersHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return UsersHolder(layoutInflater.inflate(R.layout.user_item,parent,false))
    }

    override fun onBindViewHolder(holder: UsersHolder, position: Int) {
        val item = lista[position]
        holder.render(item,context)
    }

    override fun getItemCount(): Int = lista.size


    inner class UsersHolder(val view: View): RecyclerView.ViewHolder(view) {
        var tvName: TextView = view.findViewById(R.id.tvUserItem)

        fun render(user: UsersTable, context: Context) {
            tvName.text = user.name

            itemView.setOnClickListener() {

                onClic(user, adapterPosition)

            }
        }
    }



}