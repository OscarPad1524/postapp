package com.example.postsapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.postsapp.database.DatabaseManager
import com.example.postsapp.adapters.UsersAdapter
import com.example.postsapp.database.models.*
import com.example.postsapp.databinding.ActivityMainBinding
import com.example.postsapp.retrofit.RestEngine
import com.example.postsapp.utils.AppService
import com.example.postsapp.utils.PostItem
import com.example.postsapp.utils.UserDataItem
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Call
import retrofit2.Response

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var databaseManager: DatabaseManager
    private lateinit var adapter: UsersAdapter
    private lateinit var list: List<UsersTable>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        databaseManager = DatabaseManager.getDatabase(this)
        list = mutableListOf()



        confirm()
        initRecycler()
        callServiceGetPosts()


    }

    fun confirm() {
        GlobalScope.launch {
            if (databaseManager.usersDao().getById(2) == null) {
                agregarPrimeros()
                confirm()
            } else {
                var mutable = databaseManager.usersDao().getAll()
                list = mutable.toList()
            }
        }

    }

    private fun initRecycler() {
        binding.recycler.layoutManager = LinearLayoutManager(this)
        adapter = UsersAdapter(list, this){ usersTable, i ->
            onClick(usersTable, i)
        }

        binding.recycler.adapter = adapter
    }


    private fun onClick(userTable: UsersTable, posicion: Int) {

    }

    private fun agregarPrimeros() {

        GlobalScope.launch {
            if (databaseManager.usersDao().getById(2) == null) {
                callServiceGetUsers()
                callServiceGetPosts()
            } else {
                withContext(Dispatchers.Main) {
                    Toast.makeText(this@MainActivity, "Usuarios ya agregados", Toast.LENGTH_SHORT).show()
                }
            }
        }

    }

    private fun callServiceGetPosts() {
        val userService: AppService = RestEngine.getRestEngine().create(AppService::class.java)
        val result: retrofit2.Call<List<PostItem>> = userService.listPost()

        var respuesta: List<PostItem>? = listOf()


        result.enqueue(object : retrofit2.Callback<List<PostItem>>{

            override fun onFailure(
                call: retrofit2.Call<List<PostItem>>,
                t: Throwable
            ) {
                Toast.makeText(this@MainActivity, "algo falló", Toast.LENGTH_SHORT).show()
            }


            override fun onResponse(
                call: Call<List<PostItem>>,
                response: Response<List<PostItem>>,
            ) {
                respuesta = response.body()
                respuesta


                for (i in respuesta!!) {


                     GlobalScope.launch {


                        var post = PostsTable(
                            id = 0,
                            id_user = i.userId.toLong(),
                            id_server = i.id.toLong(),
                            tittle = i.title,
                            body = i.body
                        )




                    databaseManager.postsDao().insertAll(post)
                    }


                }


            }
        }
        )
    }


    private fun callServiceGetUsers() {

        val userService: AppService = RestEngine.getRestEngine().create(AppService::class.java)
        val result: retrofit2.Call<List<UserDataItem>> = userService.listUsers()

        var respuesta: List<UserDataItem>? = listOf()


        result.enqueue(object : retrofit2.Callback<List<UserDataItem>>{
            override fun onFailure(
                call: retrofit2.Call<List<UserDataItem>>,
                t: Throwable
            ) {

            }

            override fun onResponse(
                call: retrofit2.Call<List<UserDataItem>>,
                response: Response<List<UserDataItem>>
            ) {
                respuesta = response.body()

                GlobalScope.launch {
                    for (i in respuesta!!) {

                        var user = UsersTable(
                            id_server = i.id.toString(),
                            name = i.name,
                            username = i.username,
                            email = i.email,
                            phone = i.phone,
                            website = i.website

                        )
                        var address = AddressTable(
                            id_server = user.id_server,
                            street = i.address.street,
                            suite = i.address.suite,
                            city = i.address.city,
                            zipcode = i.address.zipcode
                        )
                        var geo = GeosTable(
                            id_server = user.id_server,
                            latitud = i.address.geo.lat,
                            longitud = i.address.geo.lng
                        )

                        var company = CompanysTable(
                            id_server = user.id_server,
                            id_user = user.id,
                            name = i.company.name,
                            catch_phrase = i.company.catchPhrase,
                            bs = i.company.bs
                        )

                        user.id_address = address.id
                        address.id_geo = geo.id
                        geo.id_addres = address.id
                        company.id_user = user.id
                        user.id_company = company.id


                        databaseManager.usersDao().insert(user)
                        databaseManager.addreesDao().insert(address)
                        databaseManager.geosDao().insert(geo)
                        databaseManager.companysDao().insert(company)


                    }
                }

            }
        }

        )


    }
}