package com.example.postsapp.utils

import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Path

interface AppService {

    @GET("users")
    fun listUsers(): retrofit2.Call<List<UserDataItem>>

    @GET("posts")
    fun listPost(): retrofit2.Call<List<PostItem>>

    @GET("posts?userId={id}")
    fun listPostByUser(@Path("id") id:Int, @Body post: PostItem?): retrofit2.Call<List<PostItem>>

}